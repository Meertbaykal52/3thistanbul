require('../sass/all.scss');
import './prototype';
import YasamSecenekleri from './pages/yasam-secenekleri';
import Main from './pages/index';
import Lokasyon from './pages/lokasyon';
import Galeri from './pages/galeri';
import Helper from './helper';
import AOS from 'aos';

const Pages = [
  {
    name: 'main',
    page: Main
  },
  {
    name: 'yasam-secenekleri',
    page: YasamSecenekleri
  },
  {
    name: 'lokasyon',
    page: Lokasyon
  },
  {
    name: 'galeri',
    page: Galeri
  },
];

const imgs = document.querySelectorAll('img[data-src]');
Helper.loader(imgs);
window.onload = function () {
  AOS.init();
}

const mobileButton = document.querySelector('.mobile-menu button');
const menu = document.querySelector('.header-menu__bottom--context');
mobileButton.e = ['click', (e) => {
  menu.style.display = 'flex';
}];

const currentPage = Pages.filter(item => item.name == window.page);
currentPage.length && new currentPage[0].page();