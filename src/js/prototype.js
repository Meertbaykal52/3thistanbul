const props = {
  dimension: {
    get: function () {
      return this.getBoundingClientRect();
    }
  },
  e: {
    set: function (opt) {
      if (Array.isArray(opt) && opt.length == 2) {
        this.addEventListener(opt[0], opt[1], false);
      }
    }
  },
  css: {
    set: function (opts) {
      Object.keys(opts).forEach(key => this.style[key] = opts[key]);
    }
  },
  index: {
    get: function () {
      return Array.prototype.slice.call(this.parentNode.children).indexOf(this);
    }
  },
  visible: {
    set: function (value) {
      this.style.display = value === true ? 'block' : 'none';
    }
  },
  overflow: {
    set: function (value) {
      this.style.overflow = value;
    }
  }
};

const arrayProps = {
  each: function (action) {
    for (let i = 0; i < this.length; i++) {
      action && action(this[i], i);
    }
  }
};

Object.keys(arrayProps).forEach(key => {
  Array.prototype[key] = arrayProps[key];
  HTMLCollection.prototype[key] = arrayProps[key];
});

['touchstart', 'touchend', 'touchmove', 'click', 'mouseup', 'mousedown'].forEach(item => {
  props[item] = {
    set: function (action) { this.addEventListener(item, action, false); }
  };
});
Object.defineProperties(Element.prototype, props);
