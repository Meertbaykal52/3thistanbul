<?php

/** Genel Rotalar */
Router::get('', 'HomeController', 'index');
Router::get('/daire-planlari', 'HomeController', 'daireplanlari');
Router::get('/basinda', 'HomeController', 'basinda');
Router::get('/lokasyon', 'HomeController', 'lokasyon');
Router::get('/iletisim', 'HomeController', 'iletisim');
Router::get('/galeri', 'HomeController', 'galeri');
Router::get('/yasam-secenekleri', 'HomeController', 'yasamsecenekleri');

